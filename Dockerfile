FROM golang:1.17-alpine AS build

ENV CGO_ENABLED=0 GOOS=linux
WORKDIR /go/src/app
COPY . .
# build the analyzer binary and automatically set the AnalyzerVersion
# variable to the most recent version from the CHANGELOG.md file
RUN CHANGELOG_VERSION=$(grep -m 1 '^## v.*$' "CHANGELOG.md" | sed 's/## v//') && \
        PATH_TO_MODULE=`go list -m` && \
        go build -ldflags="-X '$PATH_TO_MODULE/metadata.AnalyzerVersion=$CHANGELOG_VERSION'" -o analyzer

# Install njsscan
FROM python:3.7-alpine

ARG SCANNER_VERSION
ENV SCANNER_VERSION ${SCANNER_VERSION:-0.2.8}

RUN pip install njsscan==$SCANNER_VERSION

RUN apk --no-cache add git ca-certificates gcc libc-dev

COPY --chown=root:root --from=build /go/src/app/analyzer /
COPY .njsscan .njsscan

ENTRYPOINT []
CMD ["/analyzer", "run"]
