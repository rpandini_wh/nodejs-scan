package main

import (
	"encoding/json"
	"io"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/command"
	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/metadata"
	report "gitlab.com/gitlab-org/security-products/analyzers/report/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"

	log "github.com/sirupsen/logrus"
)

// NodeJSScanFile contains information about the file where a njsscan vulnerability is present
type NodeJSScanFile struct {
	FilePath   string `json:"file_path"`
	MatchLines []int  `json:"match_lines"`
	// MatchString   string `json:"match_string"`
}

// NodeJSScanMetadata contains information about a njsscan vulnerability
type NodeJSScanMetadata struct {
	Cwe         string `json:"cwe"`
	Description string `json:"description"`
	Severity    string `json:"severity"`
}

// NodeJSScanFinding is a container for njsscan vulnerabilities which could be either `nodejs` findings or `templates` findings
type NodeJSScanFinding struct {
	Files    []NodeJSScanFile   `json:"files"`
	Metadata NodeJSScanMetadata `json:"metadata"`
}

// NodeJSScanResult represents the output from a njsscan
type NodeJSScanResult struct {
	Errors []struct {
		Type    string `json:"type"`
		LongMsg string `json:"long_msg"`
	} `json:"errors"`
	Nodejs    map[string]NodeJSScanFinding `json:"nodejs"`
	Templates map[string]NodeJSScanFinding `json:"templates"`
}

func convert(reader io.Reader, projectPath string) (*report.Report, error) {
	var (
		results NodeJSScanResult
		vulns   []report.Vulnerability
	)
	err := json.NewDecoder(reader).Decode(&results)
	if err != nil {
		return nil, err
	}

	// since njsscan reports findings using the absolute path we need to
	// check if projectPath is "." and set is as `CI_PROJECT_DIR` so the paths
	// reported in the artifact are relative to the project root.
	if projectPath == "." {
		projectPath = os.Getenv(command.EnvVarCIProjectDir)
	}

	// Log any njsscan errors to debug if encountered.
	if len(results.Errors) != 0 {
		for _, err := range results.Errors {
			log.Debugf("njsscan error: %s, %s", err.Type, err.LongMsg)
		}
	}

	ruleIDs, findings := concatFindings(results.Nodejs, results.Templates)
	for _, ruleID := range ruleIDs {
		rule := findings[ruleID]
		for _, f := range rule.Files {
			if len(f.MatchLines) != 2 {
				log.Warnf("skipping finding %s due to incomplete njsscan data", ruleID)
				continue
			}
			filePath, err := relativeToProjectPath(projectPath, f.FilePath)
			if err != nil {
				return nil, err
			}
			cweID := strings.Split(rule.Metadata.Cwe, " ")[0]
			cweID = strings.Split(cweID, ":")[0]
			vulns = append(vulns, report.Vulnerability{
				Category:    metadata.Type,
				Name:        ruleID,
				Message:     truncate(rule.Metadata.Description, maxMsgLen),
				Description: truncate(rule.Metadata.Description, maxMsgLen),
				CompareKey:  strings.Join([]string{filePath, strconv.Itoa(f.MatchLines[0]), ruleID, cweID}, ":"),
				Severity:    severityToLevel(rule.Metadata.Severity),
				Scanner:     metadata.IssueScanner,
				Location: report.Location{
					File:      filePath,
					LineStart: f.MatchLines[0],
					LineEnd:   f.MatchLines[1],
				},
				Identifiers: []report.Identifier{
					{
						Type:  "njsscan_rule_type",
						Name:  ruleID,
						Value: truncate(rule.Metadata.Description, maxMsgLen),
					},
					{
						Type:  report.IdentifierTypeCWE,
						Name:  cweID,
						Value: ruleID,
					},
				},
			})
		}
	}
	newReport := report.NewReport()
	newReport.Analyzer = "nodejs-scan"
	newReport.Config.Path = ruleset.PathSAST
	newReport.Vulnerabilities = vulns
	return &newReport, nil
}

// severityToLevel maps a sempgrep severity level to a SAST report severity level. The three options that
// njsscan use are "INFO", "WARNING", and "ERROR".
func severityToLevel(severity string) report.SeverityLevel {
	switch severity {
	case "INFO":
		return report.SeverityLevelInfo
	case "WARNING":
		return report.SeverityLevelMedium
	case "ERROR":
		return report.SeverityLevelHigh
	}
	return report.SeverityLevelUnknown
}

// concatFindings concatenates NodeJSScanFindings and returns a sorted list of the findings keys, which are njsscan's ruleIDs.
func concatFindings(findings ...map[string]NodeJSScanFinding) ([]string, map[string]NodeJSScanFinding) {
	var keys []string
	ret := make(map[string]NodeJSScanFinding)
	for _, f := range findings {
		for k, v := range f {
			keys = append(keys, k)
			ret[k] = v
		}
	}
	sort.Strings(keys)
	return keys, ret
}

// relativeToProjectPath attempts to make filePath relative to projectPath. If projectPath is not set
// filePath is returned. Otherwise, the relative path to projectPath is returned.
func relativeToProjectPath(projectPath string, filePath string) (string, error) {
	if projectPath == "" {
		return filePath, nil
	}
	rel, err := filepath.Rel(projectPath, filePath)
	if err != nil {
		return filePath, nil
	}
	return cleanPath(rel), nil
}

func truncate(str string, maxLen int) string {
	if len(str) > maxLen {
		return str[:maxLen-1]
	}
	return str
}

// CleanPath makes a path safe for use with filepath.Join. This is done by not
// only cleaning the path, but also (if the path is relative) adding a leading
// '/' and cleaning it (then removing the leading '/'). This ensures that a
// path resulting from prepending another path will always resolve to lexically
// be a subdirectory of the prefixed path. This is all done lexically, so paths
// that include symlinks won't be safe as a result of using CleanPath.
//
// This function comes from runC (libcontainer/utils/utils.go):
// https://github.com/opencontainers/runc/blob/d636ad6256f9194b0f4c6ee181e75fb36e3446d8/libcontainer/utils/utils.go#L53
func cleanPath(path string) string {
	// Deal with empty strings nicely.
	if path == "" {
		return ""
	}

	// Ensure that all paths are cleaned (especially problematic ones like
	// "/../../../../../" which can cause lots of issues).
	path = filepath.Clean(path)

	// If the path isn't absolute, we need to do more processing to fix paths
	// such as "../../../../<etc>/some/path". We also shouldn't convert absolute
	// paths to relative ones.
	if !filepath.IsAbs(path) {
		path = filepath.Clean(string(os.PathSeparator) + path)
		// This can't fail, as (by definition) all paths are relative to root.
		path, _ = filepath.Rel(string(os.PathSeparator), path)
	}

	// Clean the path again for good measure.
	return filepath.Clean(path)
}
