# NodeJsScan analyzer

This is a GitLab analyzer for NodeJS projects. The NodeJsScan analyzer is a wrapper around [njsscan](https://github.com/ajinabraham/njsscan), a tool that checks NodeJS code for CWEs based on [semgrep](https://github.com/returntocorp/semgrep) rules. 


This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## Versioning and release process

Please check the [Release Process documentation](https://gitlab.com/gitlab-org/security-products/release/blob/master/docs/release_process.md).

## Updating the underlying Scanner
Check if there is an updated [version of njsscan](https://pypi.org/project/njsscan/). If there is, update the version in the Dockerfile with the desired version:
```dockerfile
RUN pip install njsscan=={desired njsscan version}
```


## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT Expat license, see the [LICENSE](LICENSE) file.
