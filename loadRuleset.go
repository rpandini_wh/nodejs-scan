package main

import (
	"path/filepath"

	"gitlab.com/gitlab-org/security-products/analyzers/nodejs-scan/v2/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/ruleset"

	log "github.com/sirupsen/logrus"
)

func loadRuleset(projectPath string) (string, error) {
	// Load custom ruleset if available
	rulesetPath := filepath.Join(projectPath, ruleset.PathSAST)
	rulesetConfig, err := ruleset.Load(rulesetPath, metadata.AnalyzerID)
	if err != nil {
		switch err.(type) {
		case *ruleset.NotEnabledError:
			log.Debug(err)
		case *ruleset.ConfigFileNotFoundError:
			log.Debug(err)
		case *ruleset.ConfigNotFoundError:
			log.Debug(err)
		case *ruleset.InvalidConfig:
			log.Fatal(err)
		default:
			return "", err
		}
	}

	if rulesetConfig != nil && len(rulesetConfig.Passthrough) != 0 {
		return ruleset.ProcessPassthroughs(rulesetConfig, log.StandardLogger())
    }

	return njsscanConfig, nil
}
