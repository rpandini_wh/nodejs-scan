package main

import (
	"bytes"
	"io"
	"io/ioutil"
	"os"
	"os/exec"
	"syscall"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"
)

const (
	maxMsgLen          = 150
	njsscanConfig      = ".njsscan"
	reportPath         = "/tmp/njsscan.json"
	njsscanEmptyReport = `
{
  "errors": [],
  "nodejs": {},
  "templates": {}
}
`
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, projectPath string) (io.ReadCloser, error) {
	configFile, err := loadRuleset(projectPath)
	if err != nil {
		return nil, err
	}

	cmd := exec.Command("njsscan", "--config", configFile, "--json", "--output", reportPath, projectPath)
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	if exitErr, ok := err.(*exec.ExitError); ok {
		if exitErr.Sys().(syscall.WaitStatus).ExitStatus() == 1 {
			return os.Open(reportPath)
		}
		return nil, exitErr
	}

	// no vulnerabilities found so return an empty njsscan report
	return ioutil.NopCloser(bytes.NewReader([]byte(njsscanEmptyReport))), nil
}
