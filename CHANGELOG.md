# NodeJsScan analyzer changelog

## v2.20.0
- Bump ruleset module (!108)

## v2.19.3
- Update common to `v2.24.1` which fixes a git certificate error when using `ADDITIONAL_CA_CERT_BUNDLE` (!109)

## v2.19.2
- chore: Update go to v1.17 (!107)

## v2.19.1
- chore: Use ruleset.ProcessPassthrough helper (!106)

## v2.19.0
- Update njsscan to [v0.2.8](https://github.com/ajinabraham/njsscan/releases/tag/0.2.8) (!104)
    - Support njsscan-ignore for templates
    - deprecate ignore:
    - semgrep update
    - CWE Typo Fix
    - libsast pattern matcher to support ignore findings.

## v2.18.0
- Update njsscan to [v0.2.6](https://github.com/ajinabraham/njsscan/releases/tag/0.2.6) (!98)
    - License Change: LGPL2.1 -> LGPL3.0+
    - Semgrep bump
    - Support HTML output format

## v2.17.0
- Update njsscan to [v0.2.4](https://github.com/ajinabraham/njsscan/releases/tag/0.2.4) (!95)
    - Bump Semgrep version to 0.45
    - Update Max Scan file size from 25 to 5 MB.
    - Added New Sequelize Rules from Semgrep


## v2.16.0
- Update report dependency in order to use the report schema version 14.0.0 (!95)

## v2.15.0
- Update njsscan to [v0.2.3](https://github.com/ajinabraham/njsscan/releases/tag/0.2.3) (!94)
    - Skip files > 25MB

## v2.14.0
- Update njsscan to [v0.2.2](https://github.com/ajinabraham/njsscan/releases/tag/0.2.2) (!91)
    - New Rule Express hbs Local File Read
    - Rule QA
    - New config --config to support .njsscan file from a custom location
    - Replaced expires rule and maxAge rule

## v2.13.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!89)

## v2.13.0
- Update njsscan to v0.1.9 (!88)
  - Added rule to detect CWE-89 (SQL Injection) with knex.js.
  - Added rule to detect CWE-327 (broken or risky crypto algorithm) when AES is used without an initialization vector.

## v2.12.1
- Fix `scanner.id` to `nodejs-scan` (!87)

## v2.12.0
- Update common to v2.22.0 (!86)
- Update urfave/cli to v2.3.0 (!86)

## v2.11.0
- Update njsscan to v0.1.8 (!84)
- Update logrus, cli golang dependencies

## v2.10.1
- Update common library to fix a null bug, path bug, and add custom errors (!83)

## v2.10.0
- Update analyzer to use [njsscan](https://github.com/ajinabraham/njsscan) (!80)
- Replaces old [rules](https://github.com/ajinabraham/nodejsscan/blob/v3.7/core/rules.xml) with [100+ new rules](https://github.com/ajinabraham/njsscan/tree/master/njsscan/rules) based on semgrep. (!80)
- Changes base Docker image from `node:14-alpine3.12` to `python:3.7-alpine` (!80)

## v2.10.0
- Update common library to include rule disablement (!82)

## v2.9.6
- Warn if no files match instead of returning error (!79)

## v2.9.5
- Fix bug which prevented writing `ADDITIONAL_CA_CERT_BUNDLE` value to `/etc/gitconfig` (!77)

## v2.9.4
- Update common library and golang version (!76)

## v2.9.3
- Update node to v14.11 (!75)

## v2.9.2
- Update node and supporting dependencies (!72)

## v2.9.1
- Update node and supporting dependencies (!67)

## v2.9.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!70)

## v2.8.1
- Upgrade go to version 1.15 (!69)

## v2.8.0
- Add scan object to report (!64)

## v2.7.2
- Add `app.Version` to support `scan.scanner.version` field in security reports (!63)

## v2.7.1
- Bump JS dependencies to latest versions (!61)

## v2.7.0
- Switch to the MIT Expat license (!60)

## v2.6.0
- Add `SAST_DISABLE_BABEL` flag (!58)

## v2.5.0
- Update logging to be standardized across analyzers (!59)

## v2.4.1
- Remove `location.dependency` from the generated SAST report (!56)

## v2.4.0
- Update third-party dependencies to latest versions (!40)

## v2.3.0
- Add `id` field to vulnerabilities in JSON report (!31)

## v2.2.0
- Add support for custom CA certs (!27)

## v2.1.1
- Allow babel to run from anywhere in the FS, to fix a bug in non-DinD mode (!19)

## v2.1.0
- Upgrade Babel from 6 to 7 (!20)

## v2.0.2
- Set full default path for `rules.xml` to avoid error when running without Docker-in-Docker (!17)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Switch to new report syntax with `version` field

## v1.2.0
- Add an `Identifier` generated from the NodeJsScan's rule name

## v1.1.0
- Add `Scanner` property and deprecate `Tool`

## v1.0.0
- Initial release
